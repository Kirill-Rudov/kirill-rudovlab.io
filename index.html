---
layout: default
title: Home
---

<p class="intro">
 I am a <strong>CAMSE Postdoctoral Scholar</strong> in the Department of Economics at <strong>UC Berkeley</strong>. My research is in <strong>microeconomic theory</strong>, with a focus on market design. I graduated from Princeton University with a Ph.D. in Economics in May 2023. <strong>I am on the 2024/25 job market.</strong>
</p>

<p class="intro">
<strong>Talks include:</strong> Stony Brook GT Conference, session organizer (July 2024); Stanford GSB (November 2024); CLIMB Workshop (November 2024); UC Riverside (December 2024).
</p>

<p class = "paper-type">
Publications
</p>

<div class = "paper-description">
  <a href="papers/centralized_matching.pdf" class="paper-title" title="Centralized Matching with Incomplete Information">Centralized Matching with Incomplete Information</a>,
          <span>
            2022, with <a href="https://www.marcelo-fernandez.com/" class="paper-coauthors">Marcelo A. Fernandez</a> and
            <a href="https://lyariv.mycpanel.princeton.edu/index.htm" class="paper-coauthors">Leeat Yariv</a>,
          </span>
		  <span>
		  <span class="paper-journal">American Economic Review: Insights</span>, Volume 4(1), 18-33.
		  </span> 
		  <div class = "paper-link-line">
		<button class="collapsible" id="centralized_matching">abstract</button> 
		<span>&verbar;</span>
		<a href="papers/centralized_matching.pdf" class = "paper-links">pdf</a>  
		<span>&verbar;</span>
		<a href="papers/centralized_matching_oa.pdf" class = "paper-links">appendix</a>
			

		<div class="content-collapsible" id="centralized_matchingdata" markdown="1">
		<div class="content-margins">
		<p><span class="paper-abstract">Abstract.</span> We study the impacts of incomplete information on centralized one-to-one matching 
		markets. We focus on the commonly used Deferred Acceptance mechanism (Gale and 
		Shapley, 1962). We show that many complete-information results are fragile to a small 
		infusion of uncertainty about others' preferences.</p>
		</div>
		</div>
	</div>
		 
</div>


<p class = "paper-type">
Working Papers
</p>

<div class = "paper-description">
  <a href="papers/JMP_Kirill_Rudov.pdf" class="paper-title" title="Stable Matchings with Switching Costs">Stable Matchings with Switching Costs</a>, <span> with Boris Pittel</span>, <span class="paper-journal">Job Market Paper</span>.
     <div class = "paper-link-line">
		<button class="collapsible" id="switching_costs">abstract</button>
		<span>&verbar;</span>	
		<a href="papers/JMP_Kirill_Rudov.pdf" class = "paper-links">pdf</a> 

		
		<div class="content-collapsible" id="switching_costsdata" markdown="1">
		<div class="content-margins">
		<p><span class="paper-abstract">Abstract.</span> Traditional matching theory, and its canonical stability notion, 		assumes that agents can freely and costlessly switch partners. Without switching costs, large matching markets have a 		small number of stable matchings. In reality, however, switching partners is often costly: in labor markets, employees 		may need to move; in marriage markets, divorce frequently carries financial and emotional burdens. We study the 		impacts of switching costs and find that they can dramatically expand the set of stable matchings, even in large 		markets, and with vanishingly small costs. We precisely characterize the threshold of switching costs that triggers 		transformative expansion: an explosion in the number of stable matchings. From a market design 		perspective, accounting for switching costs, stability allows for significantly more room for policy interventions 		than previously thought. Our results provide insights into competitive forces in markets with imbalances between 		supply and demand.</p>
		</div>
		</div>
	</div>

</div>


<div class = "paper-description">
	<a href="papers/fragile_stable.pdf" class="paper-title" title="Fragile Stable Matchings">Fragile Stable Matchings.</a>	
	<div class = "paper-link-line">
		<button class="collapsible" id="fragile_stable_matchings">abstract</button>
		<span>&verbar;</span>
		<a href="papers/fragile_stable.pdf" class = "paper-links">pdf</a> 
		<span>&verbar;</span>	
		<a href="papers/fragile_stable_oa.pdf" class = "paper-links">appendix</a>

		<div class="content-collapsible" id="fragile_stable_matchingsdata" markdown="1">
		<div class="content-margins">
		<p><span class="paper-abstract">Abstract.</span> 
		We show how fragile stable matchings are in a decentralized one-to-one matching setting. 
		The classical work of Roth and Vande Vate (1990) suggests simple decentralized dynamics in which randomly-chosen 
		blocking pairs match successively. Such decentralized interactions guarantee convergence 
		to <i>a</i> stable matching. Our first theorem shows that, under mild conditions, 
		<i>any</i> unstable matching<span>&#8212;</span>including a small perturbation of a stable matching<span>&#8212;</span>can 
		culminate in <i>any</i> stable matching through these dynamics. 
		Our second theorem highlights another aspect of fragility: stabilization may take a long time. 
		Even in markets with a unique stable matching, where the dynamics always converge to the same matching, 
		decentralized interactions can require an exponentially long duration to converge. 
		A small perturbation of a stable matching may lead the market away from stability 
		and involve a sizable proportion of mismatched participants for extended periods. 
		Our results hold for a broad class of dynamics.</p>
		</div>
		</div>
	</div>

</div>

<div class = "paper-description">
<a href="papers/extreme_equilibria.pdf" class="paper-title">Extreme Equilibria: The Benefits of Correlation</a>,
          <span>
            with <a href="https://fedors.info/" class="paper-coauthors">Fedor Sandomirskiy</a> and
            <a href="https://lyariv.mycpanel.princeton.edu/index.htm" class="paper-coauthors">Leeat Yariv</a>.
          </span>
	<div class = "paper-link-line">
		<button class="collapsible" id="extreme_equilibria">abstract</button>
		<span>&verbar;</span>	
		<a href="papers/extreme_equilibria.pdf" class = "paper-links">pdf</a>
		<div class="content-collapsible" id="extreme_equilibriadata" markdown="1">
		<div class="content-margins">
		<p><span class="paper-abstract">Abstract.</span> We study whether a given Nash equilibrium can be improved within the set of correlated equilibria for arbitrary objectives. Our main contribution is a sharp characterization: in a generic game, a Nash equilibrium is an extreme point of the set of correlated equilibria if and only if at most two agents randomize. Consequently, any sufficiently mixed Nash equilibrium involving at least three randomizing agents can always be improved by correlating actions or switching to a less random equilibrium, regardless of the underlying objective. We show that even if one focuses on objectives that depend on payoffs, excess randomness in equilibrium implies improvability. We extend our analysis to symmetric games, incomplete information games, and coarse correlated equilibria, revealing a fundamental tension between the randomness in Nash equilibria and their optimality.</p>
		</div>
		</div>
		
		
		
	</div>

</div>


<div class = "paper-description">
<a href="papers/decentralized_foundation.pdf" class="paper-title" title="Decentralized Foundation for Stability of Supply Chain
Networks">Decentralized Foundation for Stability of Supply Chain
Networks.</a>
	<div class = "paper-link-line">
		<button class="collapsible" id="decentralized_foundation">abstract</button>
		<span>&verbar;</span>	
		<a href="papers/decentralized_foundation.pdf" class = "paper-links">pdf</a> 

		<div class="content-collapsible" id="decentralized_foundationdata" markdown="1">
		<div class="content-margins">
		<p><span class="paper-abstract">Abstract.</span> We propose simple dynamics that generate a stable supply chain 		network. We prove that any unstable network can reach a stable network through decentralized interactions where 		randomly-selected blocking chains form successively. Our proof suggests an algorithm for finding a stable network 
		that generalizes the classical Gale and Shapley (1962) deferred acceptance algorithm.</p>
		</div>
		</div>
	</div>

</div>


<div class = "paper-description">
  <a href="papers/dominance_solvability.pdf" class="paper-title" title="Dominance Solvability in Random Games">Dominance Solvability in Random Games</a>,
          <span>
            with <a href="https://web.math.princeton.edu/~nalon/" class="paper-coauthors">Noga Alon</a> and
            <a href="https://lyariv.mycpanel.princeton.edu/index.htm" class="paper-coauthors">Leeat Yariv</a>.
          </span>
		  <span>
		  <span class="paper-journal"></span>
		  </span> 
			<div class = "paper-link-line">
		<button class="collapsible" id="dominance_solvability">abstract</button>
		<span>&verbar;</span>
		<a href="papers/dominance_solvability.pdf" class = "paper-links">pdf</a>  
		<span>&verbar;</span>	
		<a href="papers/dominance_solvability_oa.pdf" class = "paper-links">appendix</a>
			

		<div class="content-collapsible" id="dominance_solvabilitydata" markdown="1">
		<div class="content-margins">
		<p><span class="paper-abstract">Abstract.</span> We study the effectiveness of iterated elimination of strictly 		dominated actions in normal-form games within a random games framework. Our results show that the likelihood of 		dominance solvability diminishes rapidly as the action set of at least one player expands. Moreover, when games are 		dominance-solvable, the number of iterations required to reach a Nash equilibrium increases significantly with the 		size of the action sets. However, in highly imbalanced games, iterated elimination can still greatly simplify the 		analysis by excluding a large fraction of actions. From a technical perspective, we demonstrate the value of recent 		combinatorial methods for analyzing general games.</p>
		</div>
		</div>
	</div>
</div>


<p class = "paper-type">
Work in Progress
</p>

<div class = "paper-description">
  <span class="paper-title">Fragile Stable Supply Chain Networks.</span>
          <span>  
          </span>
</div>



<div class = "paper-description">
  <span class="paper-title">Searching by Trial and Error with Correlated Sources.</span>
          <span>
          </span>
</div>

<p class="paper-type">Teaching</p>

<p class="intro" style="margin-bottom: 0;">
I have greatly enjoyed teaching a variety of courses to students at all levels, serving as an instructor, teaching assistant, and tutor. My passion for teaching comes from my mother and grandmother, both high school teachers. Please <button class="collapsible" id="teaching">click here</button>to view the courses.
</p>
		
		<div class="content-collapsible" id="teachingdata" markdown="1">
		<div class="content-margins">
		<p>Teaching Assistant, <span class="paper-abstract">Princeton University</span>, 2019<span>&#8211;</span>2021: 				Microeconomic Theory: A Mathematical Approach [UG], Microeconomic Theory [UG]</p>
		<p>Instructor, <span class="paper-abstract">New Economic School</span>, Fall 2023: Matching Theory [G]</p>
		<p>Teaching Assistant, <span class="paper-abstract">New Economic School</span>, 2015<span>&#8211;</span>2017: 				Microeconomics I<span>&#8211;</span>V [G], Microeconomics in Finance [G], Political Economics II [G], Institutional 			Economics [G], Asset Pricing [G], Investments [G], <span class="paper-abstract">Best TA Award (2016, 2017)</span></p>
		<p>Teaching Assistant, <span class="paper-abstract">HSE University</span>, Fall 2016: Decision Theory [UG], <span 			class="paper-abstract">Best TA Award</span></p>
		</div>
		</div>

